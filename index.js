const express = require('express');
const bodyParser = require('body-parser');
const fetech = require('isomorphic-fetch');
const cors = require('cors');
const app = express();


app.use(bodyParser.text({ limit: '5mb' }));
app.use(cors());

app.post('*', (req, res) => {
    sendToAPI(req.query.access_token, req.body, req.path)
    .then(data => {
        res.send(data);
    })
    .catch(err => {
        res.send(err.status, err.statusText);
    })
});

function sendToAPI(accessToken, body, path) {
    let initObject = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + accessToken,
        },
        method: 'post',
        body: body,
    };

    let endPoint = 'https://api.buddy.works' + path;

	return fetch(endPoint, initObject)
    .then(response => {
        if (!response.ok) return Promise.reject({
        	status: response.status,
            error: response.statusText,
            response,
        });
        return response.json();
    })
	.then((data) => {
		return Promise.resolve(data);
	})
}

const server = app.listen(3003, () => {
	console.log('Listening on 3003')
});

module.exports = server;
